<?php

namespace Uthman\Tafqeet;

use Uthman\Tafqeet\Exceptions\MaximumNumberExceededException;

class Tafqeet
{
    protected $prefix;
    protected $postfix;
    protected $currency;
    protected $currencyMulti;
    protected $percentileCurrency;
    protected $percentileCurrencyMulti;
    protected $glue;

    protected $result;

    public function __construct()
    {
        $this->prefix = config('tafqeet.prefix', 'فقط');
        $this->postfix = config('tafqeet.postfix', 'لا غير');
        $this->currency = config('tafqeet.currency', 'دينار');
        $this->currencyMulti = config('tafqeet.currencyMulti', 'دنانير');
        $this->percentileCurrency = config('tafqeet.percentileCurrency', 'فلساَ');
        $this->percentileCurrencyMulti = config('tafqeet.percentileCurrencyMulti', 'فلسات');
        $this->glue = config('tafqeet.glue', 'و');
    }

    public function parse($amount) : string
    {
        // validate
        $amount = floatval($amount);

        if ($amount > 10e12 - 1) {
            throw new MaximumNumberExceededException('Maximum number exceeded.');
        }

        $localCurrency = $this->currency;
        $localPercentileCurrency = $this->percentileCurrency;

        $wholePart = $this->getWholePart($amount);
        $singleLevel = mb_strlen($wholePart) <= 3;

        $res = [];
        $level = 0;

        while (mb_strlen($wholePart) > 0) {
            $portion = intval(substr($wholePart, -3));

            $res[] = $this->parseLevel($portion, $level, $singleLevel);

            if ($level == 0 && ($portion % 100 > 2 && $portion % 100 < 11)) {
                $localCurrency = $this->currencyMulti;
            }

            $wholePart = substr($wholePart, 0, max(0, mb_strlen($wholePart) - 3));
            $level += 1;
        }

        $res = array_reverse($res);
        $wholePartParsed = sprintf('%s %s', implode(" {$this->glue}", array_filter($res)), $localCurrency);

        $fractionalPart = intval($this->getFractionalPart($amount));

        if ($fractionalPart) {
            if ($fractionalPart % 100 > 2 && $fractionalPart % 100 < 11) {
                $localPercentileCurrency = $this->percentileCurrencyMulti;
            }
            $fractionalPartParsed = sprintf(
                " {$this->glue}%s %s",
                $this->parseLevel($fractionalPart), $localPercentileCurrency
            );
        }

        return sprintf(
            '%s %s%s %s',
            $this->prefix,
            $wholePartParsed,
            $fractionalPartParsed ?? '',
            $this->postfix
        );
    }

    protected function parseWhole($number)
    {
        $res = '';

        if ($number >= 10e9) {
            $n = $number / 10e9;
        }
    }

    public function parseLevel($number, $level = 0, $singleLevel = 0)
    {
        $res = [];

        $appendex = ($level) ? $this->levelAppendecies[$level][1] : '';

        // special case ZERO
        if ($number == 0 && $level == 0) {
            if ($singleLevel) {
                return $this->ones[0];
            }

            return '';
        }

        // special case handle 1 and 2
        if ($level > 0 && ! $singleLevel && ($number == 1 || $number == 2)) {
            return $this->levelAppendecies[$level][$number];
        }

        $hundredsPart = intval($number / 100);
        if ($hundredsPart) {
            $res[] = $this->getHundreds($hundredsPart);
        }

        // special case if tens is less than 12
        $tempTens = $number % 100;
        if ($tempTens > 0 && $tempTens <= 12) {
            $res[] = $this->ones[$tempTens];
            $number -= $tempTens;
            // add appropriate appendex
            if ($tempTens > 2 && $tempTens < 11) {
                $appendex = ($level) ? $this->levelAppendecies[$level][-1] : $appendex;
            }
        }

        // special case if tens is in [13 - 19]
        if ($number % 100 > 12 && $number % 100 < 20) {
            $res[] = $this->ones[$number % 10].' '.$this->tens[1];
            $number -= $number % 100;
        }

        // handle ones part
        $onesPart = $number % 10;
        if ($onesPart) {
            $res[] = $this->ones[$onesPart];
            $number -= $onesPart;
        }

        // handle tens part
        $tensPart = $number % 100;
        if ($tensPart) {
            $res[] = $this->tens[$tensPart / 10];
            $number -= $tensPart;
        }

        $parsedLevel = implode(" {$this->glue}", $res);

        if (! empty($parsedLevel) && $level > 0) {
            $parsedLevel .= ' '.$appendex;
        }

        return $parsedLevel;
    }

    protected $ones = [0 => 'صفر', 1 => 'واحد', 2 => 'اثنان', 3 => 'ثلاثة', 4 => 'اربعة', 5 => 'خمسة', 6 => 'ستة', 7 => 'سبعة', 8 => 'ثمانية', 9 => 'تسعة', 10 => 'عشرة', 11 => 'احد عشر', 12 => 'اثنا عشر'];

    protected $tens = [1 => 'عشر', 2 => 'عشرون', 3 => 'ثلاثون', 4 => 'اربعون', 5 => 'خمسون', 6 => 'ستون', 7 => 'سبعون', 8 => 'ثمانون', 9 => 'تسعون',
    ];

    protected $hundreds = [
        1 => 'مائة',
        2 => 'مائتان',
        -1 => 'مائة',
    ];

    protected $levelAppendecies = [
        1 => [
            1 => 'الف',
            2 => 'الفان',
            -1 => 'آلاف',
        ],
        2 => [
            1 => 'مليون',
            2 => 'مليونان',
            -1 => 'ملايين',
        ],
        3 => [
            1 => 'مليار',
            2 => 'ملياران',
            -1 => 'مليارات',
        ],
   ];

    protected function getFractionalPart(float $amount) : string
    {
        return substr(number_format(fmod($amount, 1), 3), 2);
    }

    protected function getWholePart(float $amount) : string
    {
        return number_format($amount, 0, '', '');
    }

    protected function getHundreds($n)
    {
        $n = max(1, min($n, 9));

        if ($n <= 2) {
            return $this->hundreds[$n];
        }

        return preg_replace('/(ية|ة)$/i', '', $this->ones[$n]).$this->hundreds[-1];
    }
}
