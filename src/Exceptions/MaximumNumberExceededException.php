<?php

namespace Uthman\Tafqeet\Exceptions;

use Exception;

class MaximumNumberExceededException extends Exception
{
}
