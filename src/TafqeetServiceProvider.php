<?php

namespace Uthman\Tafqeet;

use Illuminate\Support\ServiceProvider;

class TafqeetServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Uthman\Tafqeet\Tafqeet');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/tafqeet.php' => config_path('tafqeet.php'),
        ]);
    }
}
