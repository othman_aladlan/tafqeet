<?php
/**
 * Tafqeet Configuration File.
 */

return [
    'prefix' => 'فقط',
    'postfix' => 'لا غير',
    'currency' => 'دينار',
    'currencyMulti' => 'دنانير',
    'percentileCurrency' => 'فلساَ',
    'percentileCurrencyMulti' => 'فلسات',
    'glue' => 'و',
];
