## Tafqeet

Simple package to transform arabic money numbers to words.

### Installation
```php
composer require uthman/tafqeet
```

### Usage
```php
use Uthman\Tafqeet\Tafqeet;

echo (new Tafqeet)->parse("123.456");
// فقط مائة وثلاثة وعشرون دينار واربعمائة وستة وخمسون فلساَ لا غير
```

### Configuration
*publish configuration file*
```php
php artisan vendor:publish --provider="Uthman\\Tafqeet\\TafqeetServiceProvider"
```